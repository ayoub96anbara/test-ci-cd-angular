import {Component, OnInit} from '@angular/core';
import {ProductService} from '../../services/product.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products;
  readonly host: string = ProductService.host;


  constructor(private productService: ProductService
    , private activatedRoute: ActivatedRoute
    , private router: Router
  ) {


  }


  ngOnInit(): void {
    this.router.events.subscribe(
      (event) => {
        if (event instanceof NavigationEnd) {
          // let url = value.url;
          console.log(event);
          let flag = this.activatedRoute.snapshot.params.flag;
          if (flag == 98) {
            // get All selected products
            // i will ignore idCategory param
            this.getProducts(this.host + '/products/search/bySelected');
          } else if (flag == 99) {
            // get All products by category
            let idCategory = this.activatedRoute.snapshot.params.idCategory;
            this.getProducts(this.host + `/categories/${idCategory}/products`);
          }

        }
      });

    let flag = this.activatedRoute.snapshot.params.flag;
    if (flag == 98) {
      // get All selected products
      // i will ignore idCategory param
      this.getProducts(this.host + '/products/search/bySelected');
    }


  }


  private getProducts(url: string) {
    this.productService.getProducts(url).subscribe(
      (result) => {
        console.log(result);
        this.products = result._embedded.products;
      },
      error => {
        console.error(error);
      }
    );
  }
}

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ProductService} from './product.service';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {
  constructor(private http:HttpClient) { }

  getALlCategories():Observable<any>{
   return  this.http.get(ProductService.host+'/categories')
  }
}

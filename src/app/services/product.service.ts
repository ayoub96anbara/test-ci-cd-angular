import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CatalogService} from './catalog.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  public static readonly host:string='http://localhost:8087';


  constructor(private http:HttpClient) { }

  getProducts(url:string):Observable<any>{
    return  this.http.get(url)
  }
}

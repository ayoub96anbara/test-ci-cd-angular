import {Component, OnInit} from '@angular/core';
import {CatalogService} from './services/catalog.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  categories: any;
  currentCategoryID;

  constructor(private catalogService: CatalogService,
              private router:Router
              ) {

  }

  ngOnInit(): void {
    this.getALlCategories();

  }
  getALlCategories(){
    this.catalogService.getALlCategories().subscribe(
      (result) => {
        console.log(result);
        this.categories = result._embedded.categories;
      },
      error => {
        console.error(error);
      }
    );
  }

  getProductsByCategory(id: number) {
    this.router.navigateByUrl(`products/99/${id}`);
    this.currentCategoryID=id;
  }

  onClickHome() {
    this.currentCategoryID=undefined;
    this.router.navigateByUrl('products/98/33');
  }
}

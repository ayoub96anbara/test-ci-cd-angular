import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProductsComponent} from './components/products/products.component';


const routes: Routes = [
  {path : 'products/:flag/:idCategory',component : ProductsComponent},
  {path :'' , redirectTo: 'products/98/1' ,pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
